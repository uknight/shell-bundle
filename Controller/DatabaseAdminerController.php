<?php
/**
 * Created by PhpStorm.
 * User: Varg
 * Date: 15.07.2017
 * Time: 20:24
 */

namespace Uknight\ShellBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
    Symfony\Component\HttpFoundation\Response;

class DatabaseAdminerController extends Controller
{
    /**
     * Added adminer for quick database management. Should be replaced in future.
     * See the documentation for adminer here: https://www.adminer.org
     *
     * @return Response
     */
    public function databaseAdminerAction()
    {
        return new Response(
            include_once $this->container->getParameter('kernel.root_dir') . '/../vendor/uknight/shell-bundle/Resources/views/Adminer/adminer.php'
        );
    }
}