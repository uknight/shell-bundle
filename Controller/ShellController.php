<?php

namespace Uknight\ShellBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class ShellController extends Controller
{
    /**
     * @Route("/shell/list", name="uknight_shell_list")
     *
     */
    public function listAction()
    {
        $actions = array(
            /*1 => array (
                'name' => 'Pull',
                'address' => 'uknight_shell_pull',
            ),*/
                /*2 => array(
                    'name' => 'Update',
                    'address' => 'uknight_shell_update',
                ),*/
            3 => array(
                'name' => 'Clear Cache',
                'address' => 'uknight_shell_cc',
            ),
            4 => array(
                'name' => 'Asset install',
                'address' => 'uknight_shell_alink',
            ),
        );

        $php = $this->getParameter('shell.php_path');
        exec($php . ' -v 2>&1 && '. $php . ' -i | grep -E "(memory_limit|post_max_size|max_input_time|max_execution_time|upload_max_filesize)"', $stdin);
        return $this->render('UknightShellBundle:Shell:list.html.twig', array(
            'actions' => $actions,
            'stdin' => $stdin,
            'phpParams' => array(
                'php_version'         => phpversion(),
                'memory_limit'        => ini_get('memory_limit'),
                'post_max_size'       => ini_get('post_max_size'),
                'max_input_time'      => ini_get('max_input_time'),
                'max_execution_time'  => ini_get('max_execution_time'),
                'upload_max_filesize' => ini_get('upload_max_filesize'),
            ),
        ));
    }

    /**
     * @Route("/shell/pull", name="uknight_shell_pull")
     */
    public function pullAction()
    {
//        $root = $this->getParameter('kernel.root_dir');
//        $php = $this->getParameter('shell.php_path');
        exec('git pull 2>&1', $stdin);

        return $this->render('UknightShellBundle:Shell:action.html.twig', array(
            'action' => 'Git pull',
            'stdin' => $stdin,
        ));
    }

    /**
     * @Route("/shell/update", name="uknight_shell_update")
     */
    public function updateAction()
    {
        $root = $this->getParameter('kernel.root_dir');
//        $php = $this->getParameter('shell.php_path');
        exec('cd ' . $root . '/../ && composer update 2>&1', $stdin);
        return $this->render('UknightShellBundle:Shell:action.html.twig', array(
            'action' => 'Composer update',
            'stdin' => $stdin,
        ));
    }

    /**
     * @Route("/shell/cc", name="uknight_shell_cc")
     */
    public function ccAction()
    {
        $root = $this->getParameter('kernel.root_dir');
        $php = $this->getParameter('shell.php_path');
        exec($php . ' ' . $root . '/../bin/console cache:clear --env dev && ' . $php . ' ' . $root . '/../bin/console cache:clear --env prod 2>&1', $stdin);

        return $this->render('UknightShellBundle:Shell:action.html.twig', array(
            'action' => 'Clear Cache',
            'stdin' => $stdin,
        ));
    }

    /**
     * @Route("/shell/alink", name="uknight_shell_alink")
     */
    public function aLinkAction()
    {
        $root = $this->getParameter('kernel.root_dir');
        $php = $this->getParameter('shell.php_path');
        exec('cd ' . $root . '/../ && ' . $php . ' ' . $root . '/../bin/console asset:install --symlink 2>&1', $stdin);

        return $this->render('UknightShellBundle:Shell:action.html.twig', array(
            'action' => 'Asset install',
            'stdin' => $stdin,
        ));
    }

}
