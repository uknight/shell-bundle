# U-Knight Shell Bundle

This bundle is for some shell commands, clear caches and assets install to be specific, and info about php version in console and server environments.

## Instruction

### Prerequisites

You need:

1. symfony
2. easyadmin bundle
3. brain and straight arms
4. this installation instructions

### Installation

Add to your ```composer.json``` file following lines:

```json
    ...
    "repositories": [
        {
            "url": "git@bitbucket.org:uknight/shell-bundle.git",
            "type": "git"
        }
    ]
    ...
```

```json
    "require": {
        ...
        "uknight/shell-bundle": "@dev"
    },
```

Then run

```bash
$ composer install
```

### Configuration

Add following lines to your ```AppKernel.php``` file:

```php
$bundles = [
    ...
    new Uknight\ShellBundle\UknightShellBundle(),
];
```

You need to configure ``` shell.php_path``` parameter in your ```config.yml``` in order the bundle to work.

```yaml
parameters:
    shell.php_path: PHPPATH
```

Where ```PHPPATH``` is path to your php executable e.g.: ```/usr/bin/php```

In your ```routing.yml``` add following lines:

```yaml
uknight.shell_bundle:
    resource: "@UknightShellBundle/Resources/config/routing.yml"
    prefix:   /YOUR_ADMINPANEL_PREFIX_HERE/
uknight.db:
    resource: "@UknightShellBundle/Resources/config/routing_db.yml"
    prefix:   /YOUR_SECRET_PATH_PREFIX_HERE/
```

You need to secure path ```/YOUR_ADMINPANEL_PREFIX_HERE/``` with admin authentication (this prefix must be same as secured adminpanel prefix). And ```/YOUR_SECRET_PATH_PREFIX_HERE/``` must not be secured by symfony's security to be available when the database is dropped, but needs to be unique and random to be in secure of accidental finding. 

Then you need to configure easyadmin bundle to link to shell list action like this: 

```yaml
easy_admin:
    design:
        menu:
            ...
            - { label:  'Shell', route: 'uknight_shell_list', icon: 'terminal' }
            - { label:  'Database', route: 'database_management', icon: 'database' }
            ...
```

And you can change labels of course :)

> But be careful when exposing Database management in adminpanel this may lead to unwanted user actions in database itself. It's for administrator's use only.

## Credits

Big thanks to myself, Flash from U-Knight Web Studio, for making this bundle ;)

Also big thanks to the [adminer](https://www.adminer.org) developers! May the force be with you, guys!).